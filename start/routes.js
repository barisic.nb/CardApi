'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/



const Route = use('Route')




/*Route.get('/br/:token', ({ view }) => {return view.render('resetpassword')})
Route.post('/pw/:token','AuthController.pw').as('rs')*/



/**
 * @api {post} /newcategory Create new card category.
 * @apiName newCategory
 * @apiGroup Card
 *
 * @apiParam {String} name Name of the category.
 *
 */

Route.post('/newcategory', 'CardController.newCategory').middleware(['checkJWT','auth:jwt'])

/**
 * @api {get} /category Fetch all category
 * @apiName category
 * @apiGroup Card
 *
 *
 */

Route.get('/category', 'CardController.category').middleware(['checkJWT','auth:jwt'])

/**
 * @api {post} /refreshtoken Refresh token
 * @apiName refreshToken
 * @apiGroup User
 *
 * @apiParam {String} refreshToken User refresh token.
 *
 */

Route.post('/refreshtoken', 'AuthController.refreshToken')


/**
 * @api {post} /upload/:id Upload photo.
 * @apiName upload
 * @apiGroup Upload
 *
 * @apiParam {Email} id Id of the card.
 * @apiParam {File} cardPics Upload images.
 *
 */

Route.post('/upload/:id', 'UpdateController.update').middleware(['checkJWT','auth:jwt'])



/**
 * @api {post} /registergoogle/:email Register user via Google.
 * @apiName registergoogle
 * @apiGroup Social
 *
 * @apiParam {Email} email Email of the user.
 * @apiParam {String} password Enter user password.
 *
 */

Route.post('/registergoogle/:email', 'GoogleAuthController.register')
/**
 * @api {get} /googlesocial/:id_token Login user via Google
 * @apiName googleLogin
 * @apiGroup Social
 *
 * @apiParam {String} accesToken Google acces token of the user.
 *
 */



Route.get('/googlesocial/:accesToken', 'GoogleAuthController.login')

/**
 * @api {post} /registersocial/:email Register user via Facebook.
 * @apiName registerSocial
 * @apiGroup Social
 *
 * @apiParam {Email} email Email of the user.
 * @apiParam {String} password Enter user password.
 *
 */



Route.post('/registerfacebook/:email', 'SocialAuthenticationController.register')


/**
 * @api {get} /facebooksocial/:accesToken Login user via Facebook
 * @apiName facebookLogin
 * @apiGroup Social
 *
 * @apiParam {String} accesToken Facebook Acces token of the user.
 *
 */



Route.get('/facebooksocial/:accesToken', 'SocialAuthenticationController.login')

/**
 * @api {post} /resetpasswordlink Forgot password
 * @apiName resetPasswordLink
 * @apiGroup User
 *
 * @apiParam {Email} email Email of the User. Required data.
 *
 */
Route.post('/resetpasswordlink', 'AuthController.resetPasswordLink')
/**
 * @api {password} /resetpassword/:token New password via link
 * @apiName resetPasswordToken
 * @apiGroup User
 *
 * @apiParam {String} token User unique token sent on user e-mail.
 * @apiParam {String} password Enter user new password.
 * @apiParam {String} password_confirmation  Enter the same password.
 *
 */


Route.post('/resetpassword/:token', 'AuthController.resetPassword')
/**
 * @api {post} /userpassword/:oldpassword Change your password
 * @apiName userPasswordChange
 * @apiGroup User
 *
 * @apiParam {String} old_password Enter user old password.
 * @apiParam {String} password Enter user new password.
 * @apiParam {String} password_confirmation  Enter the same password
 *
 */
Route.post('/userpassword', 'AuthController.userPassword').middleware(['checkJWT','auth:jwt'])

/**
 * @api {post} /register Register new user
 * @apiName registerUser
 * @apiGroup User
 *
 * @apiParam {String} email Email of the User. Required data.
 * @apiParam {String} password  Password of the User. Required data.
 *
 */

 Route.post('/register', 'AuthController.postRegister')

 /**
  * @api {get} /registertoken/:token Authorization user
  * @apiName authorizationUser
  * @apiGroup User
  *
  * @apiParam {String} token Unique token of the User.
  */

 Route.get('/registertoken/:token', 'AuthController.authorizationUser')


 /**
  * @api {get} /isverified Check user verificated
  * @apiName isVerified
  * @apiGroup User
  *
  */

 Route.get('/isverified', 'AuthController.isVerified').middleware(['checkJWT','auth:jwt'])


 /**
  * @api {post} /login Login user
  * @apiName login
  * @apiGroup User
  *
  * @apiParam {String} email Email of the User. Required data.
  * @apiParam {String} password  Password of the User. Required data.
  *
  */

  Route.post('/login', 'AuthController.loginUser')


  /**
   * @api {post} /postCard Create new card
   * @apiName postCard
   * @apiGroup Card
   *
   * @apiParam {String} cardname Name of the Card. Required data.
   * @apiParam {String} barcode  Barcode of the Card.
   * @apiParam {Integer} category  Category of the Card.
   * @apiParam {Datetime} validUntil  Expirtion date of the Card.
   *
   */

   Route.post('/postcard', 'CardController.postCard').middleware(['checkJWT','auth:jwt'])

/**
 * @api {delete} /deletecard/:id Delete Card
 * @apiName deleteCard
 * @apiGroup Card
 *
 * @apiParam {Number} id Card unique ID.
 *
 */

  Route.delete('/deletecard/:id', 'CardController.deleteCard').middleware(['checkJWT','auth:jwt'])

  /**
 * @api {get} /usercard Fetch user cards
 * @apiName userCard
 * @apiGroup Card
 *
 */

  Route.get('/usercard', 'CardController.userCard').middleware(['checkJWT','auth:jwt'])


  /**
   * @api {post} /editcard/:id Edit card
   * @apiName editCard
   * @apiGroup Card
   *
   * @apiParam {Number} id Card unique ID.
   * @apiParam {String} cardname Name of the Card.
   * @apiParam {String} barcode  Barcode of the Card.
   * @apiParam {Integer} category  Category of the Card.
   *
   */

  Route.post('/editcard/:id', 'CardController.editCard').middleware(['checkJWT','auth:jwt'])


  /**
   * @api {post} /shareCard/:id Share card
   * @apiName sharecard
   * @apiGroup Share
   *
   * @apiParam {Number} id Id card that wants to share.
   * @apiParam {String} shared_email Email of user to whom we want to send the card number.
   * @apiParam {Integer} category  Category of the Card.
   * @apiParam {Datetime} vaildUntil  Expirtion date of the Card.
   *
   */


  Route.post('/sharecard/:id/', 'ShareCardController.shareCard').middleware(['checkJWT','auth:jwt'])


  /**
 * @api {get} /usersharecard Fetch user shared cards
 * @apiName userShareCard
 * @apiGroup Share
 *
 *
 */

  Route.get('/usersharecard', 'ShareCardController.userShareCard').middleware(['checkJWT','auth:jwt'])



  /**
 * @api {get} /viewsharedcard Fetch shared cards
 * @apiName viewSharedCard
 * @apiGroup Share
 *
 *
 */

  Route.get('/viewsharedcard', 'ShareCardController.viewSharedCard').middleware(['checkJWT','auth:jwt'])


  /**
 * @api {delete} /deletesharedcard Delete shared cards
 * @apiName deleteSharedCard
 * @apiGroup Share
 *
 * @apiParam {Number} id Shared card unique id.
 *
 */

  Route.delete('/deletesharedcard/:id', 'ShareCardController.deleteSharedCard').middleware(['checkJWT','auth:jwt'])
