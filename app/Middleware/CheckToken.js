'use strict'

class CheckToken {
  async handle ({ request,response,auth }, next) {

    try {
   await auth.check()
  } catch (error) {
  return response.status(400).json({"response":{},"status":{"message":"Missing or invalid jwt token"}})
  }


    await next()
  }
}

module.exports = CheckToken
