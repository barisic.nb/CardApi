'use strict'

const User = use('App/Models/User')
const Card = use('App/Models/Card')
const Share = use('App/Models/Share')
const { validate } = use('Validator')
const moment = require('moment')

class ShareCardController {

  async shareCard ({request, response, params,auth}){
  //  const id = params.id
    const card = await Card.find(params.id) //id
    const user = await auth.getUser()
    const users = await User.query().where('id',user.id).distinct('id','email','is_verified','updated_at','created_at')
    if(!card){return response.status(400).json({"response":{card},"status":{"message": 'Invalid params sent'}})}
    if(user.id != card.user_id){return response.status(400).json({"response":{users},"status":{"message":"You don\'t have permission to access this card"}})}

    const allParams = request.only(['shared_email'])
    const rules =
    {
        shared_email: 'required|min:3|max:25'
        //shared_user_id: 'required'
    }

    const validation = await validate(allParams, rules)

    if(validation.fails()){
      return response.status(400).json({"response":{shared_email},"status":{"message" :'User email sent'}})
    }
    const temp = await User.query().where('email',allParams.shared_email).first()
      if(user.is_verified===1){
      if(user.id == allParams.shared_user_id){return response.status(400).json({"response":{shared_user_id},"status":{"message": 'Invalid params sent'}})}
       await Share.query().with('user').where('send_user_id',temp.id).insert({
        card_id: card.id,
        user_id: user.id,
        shared_user_id : temp.id,
        created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
        shared_email : allParams.shared_email


        })

        return response.status(200).json({"response":{card},"status": {"message": 'Card shared'}})

      }

    if(user.is_verified===0){
        return response.status(401).json({"response":{users},"status":{"message" :'Account not verified, please verify your account!'}})
      }
    }



  async userShareCard({response, request,auth}){
    const user = await auth.getUser()

    const share = await Share.query().where('user_id','=',user.id).fetch()

    await User.query().where('id',user.id).distinct('id','email','is_verified','updated_at','created_at')
    if(share){
      return response.status(200).json({"response":{share},"status":{"message":"Card shared"}})
    }

    return response.status(400).json({"response":{share},"status":{"message": 'Nothing for fetch'}})



  }


  async viewSharedCard({response, request,auth}){
    const user = await auth.getUser()

    const share = await Share.query().where('shared_user_id','=',user.id).first()


    if(!share){
      return response.status(400).json({"response":{share},"status":{"message": 'Nothing for fetch'}})
    }

    const card = await Card.query().where('id',share.card_id).fetch()


    return response.status(200).json({"response":{card},"status":{"message":"Card shared with user"}})
  }

  async deleteSharedCard({response, request, auth, params}){
    const user = await auth.getUser()
    const share= await Share.find(params.id)
    if(!share){
      return response.status(404).json({"response":{share},"status":{"message": 'Share Card don t exist'}})
    }
    if(share.user_id != user.id){
      return response.status(404).json({"response":{share},"status":{"message": "You don\'t have permission to access this card"}})
    }
    await share.delete()
    return response.status(200).json({"response":{share},"status":{"message": 'Shared Card deleted'}})
  }





}

module.exports = ShareCardController
