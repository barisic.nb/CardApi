'use strict'
const Helpers = use('Helpers')
const User = use('App/Models/User')
const Update = use('App/Models/Update')
const Card = use('App/Models/Card')
const Env = use('Env')
const crypto = use('crypto')
const uuid = use('uuid')



class UpdateController {

  async update({request,response,auth,params}){


      const user = await auth.getUser()
      const card = await Card.find(params.id)
      if(!card){
        return response.status(404).json({"response":{card},"status": {"message": 'Card don\'t exist'}})
      }

      const temp = await User.query().where('id',card.user_id).first()
      if(!temp){
        return response.status(400).json({"response":{},"status":{"message":"You don\'t have permission to access this card"}})
      }

      const cardPics = request.file('cardPics', {
          types: ['image'],
          allowedExtensions: ['jpg', 'png', 'jpeg'],
          size: '2mb'
          })


      if(!cardPics){
        return response.status(400).json({"response":{cardPics},"status":{"message":"No image to upload!"}})
      }


      const url_hash = crypto.createHash('md5').update(uuid.v4()).digest('hex')

        if(cardPics.subtype === 'jpeg')
        {cardPics.subtype = 'jpg'}

      let url = Env.get('APP_URL')
      let upload_dir = 'uploads'
      let pict_url =`${url}/${upload_dir}/${url_hash}.${cardPics.subtype}`

      const photoName= `${new Date().getTime()}.${cardPics.subtype}`
      const upFile = await cardPics.move(Helpers.resourcesPath('../public/uploads/'),{
           name: `${url_hash}.${cardPics.subtype}`
        })

        if (!cardPics.moved()) {
        return response.status(400).json({"response":{},"status":{"message" : "Card hasn't uploaded."}})
        }




      const uploadFile = await Update.create({
        user_id: user.id,
        card_id: card.id,
        picture_name: photoName,
        type : cardPics.type,
        size: cardPics.size,
        url: pict_url
      })

      card.image_url = pict_url
      await card.save()


        if(!uploadFile){
          return response.status(400).json({"response":{cardPics},"status":{"message":"No image to upload!"}})
        }




        return response.status(200).json({"response":{uploadFile,imageURL : pict_url}, "status":{"message": "Card picture added"}})
    }

  }

module.exports = UpdateController
