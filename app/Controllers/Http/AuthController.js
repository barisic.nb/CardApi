'use strict'

const User = use('App/Models/User')
const Token = use('App/Models/Token')
const { validate, sanitize, is } = use('Validator')
const Mail = use('Mail')
const Env = use('Env')
const Hash = use('Hash')
const crypto = use('crypto')
const uuid = use('uuid')
const moment = require('moment')
const Encryption = use('Encryption')



class AuthController {
/*  async pw({request,response,params}){

    const user = await User.query().where('token_pw',params.token).first()
    const allParams = request.only(['password'])
    const rules =
    {
        password:'required|min:6|max:25',
        password_confirmation: 'required_if:password|min:6|max:25|same:password'
    }

    const validation = await validate(allParams, rules)

    if(validation.fails()){
      return{message :'Invalid password'}
    }
    let br =await Hash.make(allParams.password)

      await User.query().where('id',user.id).update({
      password:br,
      token_pw:null

      })

    return response.status(200).json({"response":{},"status":{"message" :'New password set.'}})

  }

  async br({request,response,params}){
      const user = await User.query().where('token_pw',params.token).first()
      return response.redirect('rp')
  }*/

async postRegister({request,response}){

    const user = new User()

  /*  const allParams = request.all()
    const rules = {
      email:'required|email|max:255|unique:users',
      password:'required|min:6|max:25'

      //password_confirmation: 'required_if:password|min:6|max:25|same:password'
    }

    const validation = await validate(allParams, rules)*/
    const allParams = sanitize(request.post(), {
           email: 'normalize_email'
       })
    const validation = await validate(allParams, {
            email:'required|email|max:255|unique:users',
            password:'required|min:6|max:25'
        })


      console.log(allParams.email)
    if (validation.fails()){
         return response.status(400).json({"response":{},"status":{"message" :'Email already exist'}})
    }

    const token_hash = crypto.createHash('md5').update(uuid.v4()).digest('hex')

      await User.create ({
        email : allParams.email,
        password: allParams.password,
        token : token_hash
        })




          let link = `${Env.get('APP_URL')}/registertoken/${token_hash}`
          await Mail.send('email.acc',{link}, (message) => {
              message.subject('Confirm your account')
              message.from('foo@bar.com')
              message.to(allParams.email)
         })

          return response.status(200).json({"response":{},"status":{"message" :'User created, please verify your account'}})

        }
      async authorizationUser ({response,request, params, view }){

            const user = await User.query().where('token',params.token).first()
            if(!user){

              //return response.status(400).json({"response":{},"status":{"message":"Invalid token"}})
              return view.render('invalidtoken')

              //return view.render('home')
            }

            if(user.is_verified===1){return view.render('Userisverificed')}
            const temp =  user.updated_at.add(30,'m')
            const now = moment(moment().utc(true).format('YYYY-MM-DD HH:mm:ss'))


            if (temp < now) {
                await user.merge({
                token: null
              })
              await user.save()
              const token = crypto.createHash('md5').update(uuid.v4()).digest('hex')
              user.token = token
              await user.save()
              let link = `${Env.get('APP_URL')}/registertoken/${token}`
              await Mail.send('email.acc',{ link}, (message) => {
                  message.subject('Confirm your account')
                  message.from('foo@bar.com')
                  message.to(user.email)
             })
              return view.render('expire')
              //return response.status(400).json({"response":{}, "status":{"message" :'E-mail token has expired. New e-mail was sent.}'}})
            }

              await user.merge({
                is_verified: true,
                token : null
              })
              await user.save()
              return view.render('Userisverificed')
              //return response.status(200).json({"response":{},"status":{"message" :'User is verificed'}})


      }


      async resetPasswordLink ({response,request}){
          const allParams = request.all()
          const rules = {
            email:'required|email|max:255'
          }

          const validation = await validate(allParams, rules)

          if (validation.fails()){
               return response.status(200).json({"response":{},"status":{"message" :'Invalid params sent'}})
          }
          const user = await User.query().where('email',allParams.email).first()
          if(!user){return response.status(400).json({message : 'Invalid user e-mail'})}
          const token = crypto.createHash('md5').update(uuid.v4()).digest('hex')
          user.token_pw = token
          await user.save()

          let linkReset = `${Env.get('APP_URL')}/resetpassword/${token}`
          await Mail.send('email.res',{ linkReset }, (message) => {
              message.subject('Reset you password link')
              message.from('foo@bar.com')
              message.to(allParams.email)
            })


            return response.status(200).json({"response":{},"status":{"message" :'Reset password e-mail sent'}})
          }

      async userPassword ({response, request,auth}){
        const user = await auth.getUser()
        const allParams = request.all()
        const rules =
        {
            password:'required|min:6|max:25',
            password_confirmation: 'required_if:password|min:6|max:25|same:password',
            old_password:'required|min:6|max:25'
        }

        const validation = await validate(allParams, rules)

        if(validation.fails()){

          return response.status(400).json({"response":{},"status":{"message" :'Invalid password'}})
        }
        let isSame = await Hash.verify(allParams.old_password , user.password )
        if(isSame){

              let br =await Hash.make(allParams.password)

                await User.query().where('id',user.id).update({
                password:br

                })

                return response.status(200).json({"response":{},"status":{"message" :'New password set'}})
              }
        return response.status(400).json({"response":{},"status":{"message" :'Invalid password'}})


      }



      async resetPassword ({response,request,params,view}){
        const user = await User.query().where('token_pw',params.token).first()
        if(!user){
          return view.render('invalidtoken')

          //return response.status(400).json({"response":{},"status":{"message" :'Invalid user token'}})
        }
        const temp =  user.updated_at.add(30,'m')
        const now = moment(moment().utc(true).format('YYYY-MM-DD HH:mm:ss'))


        if (temp < now) {
            await user.merge({
            token_pw: null
          })
          await user.save()
          //return response.status(400).json({"response":{},"status":{"message" :'Email token has expired. Please request a new one.'}})
          return view.render('expire')
        }

              const allParams = request.only(['password'])
              const rules =
              {
                  password:'required|min:6|max:25',
                  password_confirmation: 'required_if:password|min:6|max:25|same:password'
              }

              const validation = await validate(allParams, rules)

              if(validation.fails()){
                return{message :'Invalid password'}
              }
              let br =await Hash.make(allParams.password)

                await User.query().where('id',user.id).update({
                password:br,
                token_pw:null

                })
                return view.render('rp')
              return response.status(200).json({"response":{},"status":{"message" :'New password set.'}})
      }

      async isVerified({response,auth,request}){
        const users = await auth.getUser()
        const user = await User.query().where('id',users.id).distinct('id','email','is_verified','updated_at','created_at')
        if(user.is_verified===1){
          return response.status(200).json({"response":{user}, "status": {"message": 'User is confirmed'}})
        }
        const token = crypto.createHash('md5').update(uuid.v4()).digest('hex')
        user.token = token
        await users.save()
        let link = `${Env.get('APP_URL')}/registertoken/${token}`
        await Mail.send('email.acc',{ link}, (message) => {
            message.subject('Confirm your account')
            message.from('foo@bar.com')
            message.to(users.email)
       })
          return response.status(401).json({"response": {user}, "status": {"message": 'User is not confirmed. New authorization e-mail was sent.'}})
        }


      async loginUser ({auth,response,request}){
      const {email,password} = request.all()


      const token = await auth.withRefreshToken().attempt(email,password)

      if(!token){
        return response.status(401).json({message : 'Can not find user with provided email'})
      }

      const user = await User.query().where('email',email).distinct('id','email','is_verified','updated_at','created_at')

      return response.status(200).json({"response": {token: token.token, refreshToken: token.refreshToken, user}, "status": {"message": "User logged in"}})


      }

      async refreshToken({response,request}){
        const allParams = request.only(['refreshToken'])

        const rules = {
          refreshToken : 'required'
        }

        const validation = await validate(allParams, rules)

        if (validation.fails()){
             return response.status(400).json({"response":{}, "status":{"message": 'Invalid refresh token.'}})

        }

        const decrypted = Encryption.decrypt(allParams.refreshToken)
        try {
            await auth.generateForRefreshToken(allParams.refreshToken)
      }
        catch (error) {
            return response.status(400).json({"response": {}, "status":{"message": "Invalid refresh token."}})
      }

        const token =  await auth.generateForRefreshToken(allParams.refreshToken)

      //  await Token.query().where('token', '!=',decrypted).where('user_id',user_id).delete()
        return response.status(200).json({"response": {token: token.token, refreshToken: token.refreshToken}, "status":{"message":"New jwt token"}})
      }



}

module.exports = AuthController
