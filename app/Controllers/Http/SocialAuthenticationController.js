'use strict'

const User = use('App/Models/User')
const {validate} = use('Validator')
const got = require('got')
const crypto = use('crypto')
const uuid = use('uuid')
const Mail = use('Mail')
const Env = use('Env')




class SocialAuthenticationController {
  //ally
  async login ({params, response, request,auth})
  {

    const apiUrl = `https://graph.facebook.com/v2.7`
    const profileUrl =`${apiUrl}/me?access_token=${params.accesToken}&fields=name,email`
    try {
    const fetchProfile = await got(profileUrl, {
      headers: {
        'Accept': 'application/json'
      },
      json: true
    })
   catch (error) {
     if (request.canceled){
     return response.status(400).json({"status":{},"response":{"message":"Invalid acces token"}})
     }
   }

   const fetchProfile = await got(profileUrl, {
     headers: {
       'Accept': 'application/json'
     },
     json: true
   })



    if(!fetchProfile){return response.status(400).json({"status":{},"response":{"message":"Invalid acces token"}})}
    var temp = fetchProfile.body
    temp = temp.email
    const user = await User.query().where('email',temp).distinct('id','email','is_verified','updated_at','created_at').first()


    if(!user){
      return response.status(400).json({"status":{temp},"response":{"message":"User don`t exist"}})
      }
      const token =await auth.withRefreshToken().generate(user)

      return response.status(200).json({"status":{user,token},"response":{"message":"User logged in"}})



}

async register({params, response, request}){

  const allParams = request.all()
  const rules = {
    password : 'required|min:6|max:25',
  }

  var user = await User.query().where('email',params.email).distinct('email').first()
  if(user){return response.status(400).json({"status":{user},"response":{"message":"Email alredy exist"}})}
  const validation =await validate(allParams, rules)
  if (validation.fails()){
    return response.status(400).json({"status":{},"response":{"message":"Password is to short or to long"}})}
    const token_hash = crypto.createHash('md5').update(uuid.v4()).digest('hex')

    await User.create ({
      email:params.email,
      password:allParams.password,
      token:token_hash

    })

    let link = `${Env.get('APP_URL')}/registertoken/${token_hash}`
    await Mail.send('email.acc',{link}, (message) => {
        message.subject('Confirm your account')
        message.from('foo@bar.com')
        message.to(params.email)
   })
    user = await User.query().where('email',params.email).distinct('id','email','is_verified','updated_at','created_at').first()
    return response.status(200).json({"response":{user},"status":{"message" :'User created, please verify your account'}})



}


}


module.exports = SocialAuthenticationController
