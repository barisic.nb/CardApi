'use strict'

const User = use('App/Models/User')
const Card = use('App/Models/Card')
const Share = use('App/Models/Share')
const Category  = use('App/Models/Category')
const { validate } = use('Validator')
const Update = use('App/Models/Update')



class CardController {

  async postCard ({request, response, params,auth})

    {

      const user = await auth.getUser()
      console.log(user)


      const allParams = request.all()


      const rules =
      {
        cardname:'required|min:3|max:32',
        barcode :'required',
        /*category : 'required|array|min:1|max:1',
        'category.*': 'required|integer'*/
        category : 'required'
      }

      const validation = await validate(allParams, rules)

      if (validation.fails()){
           return response.status(400).json({"response":{}, "status":{"message": 'Card not created. Cardname and barcode are requierd field.'}})

      }
      const tempCategory = await Category
          .query()
          .getCount()

      if(tempCategory < allParams.category){
        return response.status(400).json({"response":{}, "status":{"message": 'Category dosen\'t exist.'}})
      }

      if(user.is_verified === 1){
          const card = await Card.create ({
            user_id: user.id,
            cardname : allParams.cardname,
            barcode : allParams.barcode,
            categoryId : allParams.category,
            //category : allParams.category,
            validUntil : allParams.validUntil


          })
          const cardCategory = await card.category().attach(allParams.category)
          return response.status(200).json({"response":{card}, "status":{"message": 'Card created'}})
      }

      if(user.is_verified === 0){

      const temp  = await Card.query().where('user_id', user.id).getCount()
          if(temp < 5){


          const card = await Card.create ({
            user_id: user.id,
            cardname : allParams.cardname,
            barcode : allParams.barcode,
            categoryId : allParams.category,
            validUntil : allParams.validUntil


          })
          const cardCategory = await card.category().attach(allParams.category)


          return response.status(200).json({"response":{card}, "status":{"message": 'Card created'}})


          }
      return response.status(401).json({"response":{}, "status":{"message": 'You have reached the limit of 5 cards, please verify your account!'}})
    }
  }



    async userCard ({response, request, auth}){
        const user = await auth.getUser()
        /*var cards = await Card
        .query()
        .where('user_id','=',user.id)
        .with('category', (b) => {
            b.select('id')
        })
        .fetch()
        var tempCard = []*/
      /*  cards = cards.rows
        for(let i=0;i<cards.length;i++){
            tempCard.push(JSON.stringify(cards[i]))

        }

        var regex = /\y\_\i\w\"\:(\d+)?\,/gim
        var categoryId = []
        for(let i=0;i<cards.length;i++){
            categoryId[i] = tempCard[i].match(regex)
            categoryId[i] = String(categoryId[i]).replace(/y_id":/g, "")
            categoryId[i] = String(categoryId[i]).replace(/,/g, "")
        }*/
        var cardTemp = await Card
          .query()
          .where('user_id',user.id)
          .fetch()

        /*  var categoryCard = []
        for(let i=0;i<tempCard.length;i++){
          categoryCard[i]= cardTemp[i] + categoryId[i]
        }*/




    //    console.log(tempCard.rows[1])

        if(!cardTemp){return response.status(200).json({"response" : {cardTemp},"status": {"message" : 'User hasn\'t created a card'}})}
        return response.status(200).json({"response": { cardTemp }, "status":{"message":"All user card"}})


    }

    async deleteCard ({response, params, auth})
    {


      const user = await auth.getUser()
      const card = await Card.find(params.id)



      if(!card){return response.status(404).json({"response":{card},"status": {"message": 'Card don\'t exist'}})}
      if(user.id != card.user_id){return response.status(404).json({"response":{},"status":{"message": 'You don\'t have permission to access this card'}})
      }

      const updateCard = await Update
      .query()
      .where('card_id',card.id)
      .delete()

      const cardCategory = await card
      .category()
      .detach()

      await card.delete()
      return response.status(200).json({"response":{card},"status":{"message": 'Card deleted'}})
    }

    async editCard({response, params, auth, request})
    {

      const user = await auth.getUser()
      const card = await Card.find(params.id)
      if(!card){
        return response.status(404).json({message: 'Card dont\'t exist'})
      }
      if(user.id != card.user_id){return response.status(404).json({"response":{},"status":{"message": 'You don\'t have permission to access this card'}})}

      const allParams = request.all()
      const rules =
      {
        cardname:'min:3|max:32',
        barcode :'min:1|max:36'

      }

      const validation = await validate(allParams, rules)

      if (validation.fails()){
           return response.status(404).json({"response":{},"status":{"message" :'Card isn\'t edit'}})
      }



          card.cardname = allParams.cardname
          card.barcode = allParams.barcode
          card.categoryId = allParams.category
          card.validUntil = allParams.validUntil

          await card.category().detach()
          await card.category().attach(allParams.category)

          await card.save()
      return response.status(200).json({"response":{card},"status":{"message": 'Card update!'}})

    }

    async category({response,auth,request}){
      const user = await auth.getUser()
      const category = await Category
          .query()
          .select('name','id')
          .fetch()

      return response.status(200).json({"response":{category},"status":{"message":"All category."}})
    }

    async newCategory({response,auth,request}){
      const user = await auth.getUser()
      const allParams = request.all()
      const rules =
      {
        name:'required|min:3|max:32'

      }

      const validation = await validate(allParams, rules)

      if (validation.fails()){
           return response.status(400).json({"response":{}, "status":{"message": 'Enter name of the category.'}})

      }
      const category = await Category.create ({
        name: allParams.name

      })
      return response.status(200).json({"response":{category},"status":{"message":"New category created."}})

    }



  }


module.exports = CardController
