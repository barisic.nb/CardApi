'use strict'

const Model = use('Model')

class Update extends Model {
  static get table() {
      return 'updates'
    }
    static get primaryKey () {
      return 'id'
    }

    user(){
      return this.hasMany('App/Models/User')
    }
    card () {
      return this.hasMany('App/Model/Card')
   }
}



module.exports = Update
