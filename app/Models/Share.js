'use strict'

const Model = use('Model')

class Share extends Model {
  static get table() {
      return 'shares'
    }
    static get primaryKey () {
      return 'id'
    }

    user(){
      return this.hasMany('App/Models/User')
    }
    card () {
      return this.hasMany('App/Models/Card')
    }
}

module.exports = Share
