'use strict'

const Model = use('Model')

class Category extends Model {
  static get table() {
      return 'categories'
    }
    static get primaryKey () {
      return 'id'
    }

  card () {
    return this.belongsToMany('App/Models/Card').pivotTable('card_categories')
  }
}

module.exports = Category
