'use strict'

const Model = use('Model')

class Token extends Model {
  static get table() {
      return 'tokens'
    }
  user () {
   return this.belongsTo('App/Model/User')
 }
}

module.exports = Token
