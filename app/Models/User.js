'use strict'

const Model = use('Model')

class User extends Model {
  static boot () {
    super.boot()

    /**
     * A hook to hash the user password before saving
     * it to the database.
     *
     * Look at `app/Models/Hooks/User.js` file to
     * check the hashPassword method
     */
   this.addHook('beforeCreate', 'User.hashPassword')
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens () {
    return this.hasMany('App/Models/Token')
  }

  static get primaryKey () {
    return 'id'
  }
  cards () {
    return this.hasMany('App/Models/Card')
  }

  update () {
    return this.belongsToMany('App/Models/Update').pivotModel('App/Models/Update')
    //.pivotTable('update')
  }

  share () {
    return this.belongsToMany('App/Models/Share').pivotModel('App/Models/Share')
  }
  static get table () {
    return 'users'
  }
}

module.exports = User
