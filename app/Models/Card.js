'use strict'

const Model = use('Model')

class Card extends Model {
  static get table() {
      return 'cards'
    }
    static get primaryKey () {
      return 'id'
    }

    user(){
      return this.belongsTo('App/Models/User')
    }
    share () {
      return this.belongsToMany('App/Models/Share').pivotModel('App/Models/Share')
    }
    update () {
      return this.belongsToMany('App/Models/Update').pivotModel('App/Models/Update')
    }

    category (){
      return this.belongsToMany('App/Models/Category').pivotTable('card_categories')
    }
}

module.exports = Card
