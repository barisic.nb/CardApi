'use strict'


/*
|--------------------------------------------------------------------------
| CategorySeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/
//table.enum('category',['Moda','Prehrana','Obuća','Sport','Zdravlje','Kozmetika','Elektronika','Namještaj'])
const Factory = use('Factory')
const Category  = use('App/Models/Category')
class CategorySeeder {
  async run () {


    await Category.createMany([
          {
            name: 'Moda'
          },
          {
            name: 'Prehrana'
          },
          {
            name: 'Obuca'
          },
          {
            name: 'Sport'
          },
          {
            name: 'Zdravlje'
          },
          {
            name: 'Kozmetika'
          },
          {
            name: 'Elektronika'
          },
          {
            name: 'Namjestaj'
          }
        ])
  }
}

module.exports = CategorySeeder
