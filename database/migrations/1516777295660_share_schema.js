'use strict'

const Schema = use('Schema')

class ShareSchema extends Schema {
  up () {
    this.create('shares', (table) => {
      table.increments('')
      table.integer('user_id').unsigned()
      table.integer('card_id').unsigned()
      table.integer('shared_user_id').unsigned()
      table.foreign('user_id').references('users.id')
      table.foreign('card_id').references('cards.id')
      table.foreign('shared_user_id').references('users.id')
      table.string('shared_email').nullable()
      table.timestamps('created_at')

    })
  }

  down () {
    this.drop('shares')
  }
}

module.exports = ShareSchema
