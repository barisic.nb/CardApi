'use strict'

const Schema = use('Schema')

class UpdateSchema extends Schema {
  up () {
    this.create('updates', (table) => {
      table.increments()
      table.string('picture_name').nullable()
      table.integer('user_id').unsigned()
      table.integer('card_id').unsigned()
      table.string('type', 80).notNullable()
      table.integer('size')
      table.string('url',255).nullable()
      table.foreign('user_id').references('users.id')
      table.foreign('card_id').references('cards.id')
      table.timestamps()
    })
  }

  down () {
    this.drop('updates')
  }
}

module.exports = UpdateSchema
