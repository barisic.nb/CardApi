'use strict'

const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', table => {
      table.increments('id')
      table.string('email', 255).nullable().unique()
      table.string('password', 60).notNullable()
      table.string('token', 255).nullable().unique()
      table.string('token_pw', 255).nullable().unique()
      table.boolean('is_verified',1).defaultTo(false)
//      table.datetime('expire')
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
