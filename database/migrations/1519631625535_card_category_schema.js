'use strict'

const Schema = use('Schema')

class CardCategorySchema extends Schema {
  up () {
    this.create('card_categories', (table) => {
      table.increments()
      table.integer('card_id').unsigned().references('id').inTable('cards')
      table.integer('category_id').unsigned().references('id').inTable('categories')
      table.timestamps()
    })
  }

  down () {
    this.drop('card_categories')
  }
}

module.exports = CardCategorySchema
