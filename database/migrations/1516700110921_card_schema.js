'use strict'

const Schema = use('Schema')

class CardSchema extends Schema {
  up () {
    this.create('cards', (table) => {
      table.increments('id')
      table.integer('user_id').unsigned()
      table.string('cardname', 80).notNullable()
      table.string('barcode', 80).nullable()
      table.datetime('validUntil')
      table.integer('categoryId')
      table.foreign('user_id').references('users.id')
      table.string('image_url')
      table.timestamps()
    })
  }

  down () {
    this.drop('cards')
  }
}

module.exports = CardSchema
